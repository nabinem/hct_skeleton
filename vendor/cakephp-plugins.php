<?php
$baseDir = dirname(dirname(__FILE__));
return [
    'plugins' => [
        'Bake' => $baseDir . '/vendor/cakephp/bake/',
        'CakeExcel' => $baseDir . '/vendor/dakota/cake-excel/',
        'DebugKit' => $baseDir . '/vendor/cakephp/debug_kit/',
        'Josegonzalez/Upload' => $baseDir . '/vendor/josegonzalez/cakephp-upload/',
        'Migrations' => $baseDir . '/vendor/cakephp/migrations/',
        'Muffin/Trash' => $baseDir . '/vendor/muffin/trash/',
        'Proffer' => $baseDir . '/vendor/davidyell/proffer/',
        'TinyAuth' => $baseDir . '/vendor/dereuromark/cakephp-tinyauth/'
    ]
];