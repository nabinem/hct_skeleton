/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
    // The toolbar groups arrangement, optimized for two toolbar rows.
    config.toolbarGroups = [
        {name: 'clipboard', groups: ['clipboard', 'undo']},
        {name: 'editing', groups: ['find', 'selection', 'spellchecker']},
        {name: 'links'},
        {name: 'forms'},
        {name: 'tools'},
        {name: 'document', groups: ['mode', 'document', 'doctools']},
        {name: 'others'},
        '/',
        {name: 'basicstyles', groups: ['basicstyles', 'cleanup']},
        {name: 'paragraph', groups: ['list', 'indent', 'blocks', 'align', 'bidi']},
        {name: 'styles'},
        {name: 'colors'},
        {name: 'about'},
        { name: 'insert', items: [ 'Table' ] }
    ];
    
    // Remove some buttons, provided by the standard plugins, which we don't
    // need to have in the Standard(s) toolbar.
    //config.removeButtons = 'Underline,Subscript,Superscript';

    // Se the most common block elements.
    //config.format_tags = 'p;h1;h2;h3;pre';

    // Make dialogs simpler.
    //config.removeDialogTabs = 'image:advanced;link:advanced';
    config.allowedContent = 'img hr form input param pre flash br a td p span font em strong table tr th td style script iframe u s li h1 h2 h3 h4 h5 h6 ul ol div[*]{*}(*)';// add every html element you'll use in your eml tempate,I don't test if '*[*]{*}(*) will work for all html tag?
    config.extraPlugins = 'dialogadvtab,colordialog,tableresize,menu,contextmenu,tabletools,dialog,table,dialogui,colorbutton,panelbutton,panel,floatpanel,button,font,richcombo,listblock,youtube';
    // Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
    config.filebrowserBrowseUrl = Befree.webroot + 'vendors/kcfinder/browse.php?type=files';
    config.filebrowserImageBrowseUrl = Befree.webroot + 'vendors/kcfinder/browse.php?type=images';
    config.filebrowserFlashBrowseUrl = Befree.webroot + 'vendors/kcfinder/browse.php?type=flash';
    config.filebrowserUploadUrl = Befree.webroot + 'vendors/kcfinder/upload.php?type=files';
    config.filebrowserImageUploadUrl = Befree.webroot + 'vendors/kcfinder/upload.php?type=images';
    config.filebrowserFlashUploadUrl = Befree.webroot + 'vendors/kcfinder/upload.php?type=flash';
};