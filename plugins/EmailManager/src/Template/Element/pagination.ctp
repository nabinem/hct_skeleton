<div class="col-sm-5"  style="float:right;width: 40%">
    <div class="dataTables_info " id="example1_info" role="status" aria-live="polite"><?php  echo $this->Paginator->counter(
        'showing {{current}} records out of
        {{count}} records');?>
    </div>
</div>
<div class="col-sm-7 pull-right" style="width: 60%">
   <div class="dataTables_paginate paging_simple_numbers" id="example1_paginate">
       <ul class="pagination">
       <li class="paginate_button previous disabled" id="example2_previous">
       <?php   echo $this->Paginator->prev('« Previous'); ?>
       <!--<a href="#" aria-controls="example2" data-dt-idx="0" tabindex="0">Previous</a>-->
       </li>
       <li>
       <?php echo $this->Paginator->numbers(array('modulus'=>4)); ?>
       </li>
       <li class="paginate_button next" id="example2_next">
      <?php echo $this->Paginator->next('Next »'); ?>
       </li>
       </ul>
   </div>
</div>
    