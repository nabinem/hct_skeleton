 <div class="right_col index_page" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                 <h3>Reply Email<small> <a href="<?php echo $this->Url->build(['action' => 'index']); ?>"><a href="<?php echo $this->Url->build(['action' => 'index']); ?>">(Email Manager)</a></small></h3>
              </div>
              <div class="pull-right">
                <?= $this->Html->link('<i class="fa fa-envelope"></i> '. __('Email(Inbox)'),['controller'=>'emails','action' => 'index'],['class' => 'btn btn-primary btn-xs','escape' => false]) ?>
              </div>
            </div>
            <div class="clearfix"></div>
            <?= $this->Flash->render() ?>
            <?= $this->Flash->render('auth') ?>
            <?= $this->Flash->render('ckeditor') ?>
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Reply Email</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <?= $this->Form->create($email) ?>
                    <div class="row">
                        <div class="form-group">
                            <label>To :</label>
                            <?php echo $this->Form->input('email_to', array('div' => false, 'label' => false, 'class' => 'form-control','readonly')); ?>
                        </div>
                        <div class="form-group">
                            <label>Subject : </label>
                            <?php echo $this->Form->input('email_subject', array('div' => false, 'label' => false, 'class' => 'form-control')); ?>
                        </div>
                        <div class="form-group">
                            <label>Message : </label>
                            <?php echo $this->Form->hidden('email_from');
                             echo $this->Form->hidden('email_from_sign');
                             echo $this->Form->input('message' , array('div' => false, 'label' => false, 'class' => 'form-control ckeditor','id'=>'MessageContent','type'=>'textarea', 'rows'=>10, 'cols'=>10)); 
                            ?>

                        </div>
                        <div class="ln_solid"></div>
                        <div class="form-group">
                              <button id="send" type="submit" class="btn btn-success">Send</button>
                              <a href="<?php echo $this->Url->build(['action' => 'index']); ?>" class="btn btn-primary">Cancel</a>
                        </div>
                    </div>
                    <?= $this->Form->end() ?>
                </section>
            </div>
        </div>
    </div>
</div>