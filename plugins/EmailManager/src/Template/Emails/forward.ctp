<div class="right_col index_page" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
           <h3>Forward Email<small> <a href="<?php echo $this->Url->build(['action' => 'index']); ?>"><a href="<?php echo $this->Url->build(['action' => 'index']); ?>">(Email Manager)</a></small></h3>
        </div>
        <div class="pull-right">
          <?= $this->Html->link('<i class="fa fa-envelope"></i> '. __('Email(Inbox)'),['controller'=>'emails','action' => 'index'],['class' => 'btn btn-primary btn-xs','escape' => false]) ?>
        </div>
      </div>
      <div class="clearfix"></div>
      <?= $this->Flash->render() ?>
      <?= $this->Flash->render('auth') ?>
      <?= $this->Flash->render('ckeditor') ?>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Forward</h2>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <?= $this->Form->create($email , array('id'=>'forward_email')) ?>
              <div class="row">
                  <div class="form-group">
                      <label>To :</label>
                      <?php echo $this->Form->input('email_to', array('div' => false, 'label' => false, 'class' => 'form-control','value'=>'','placeholder'=>'Ex: to@domain.com,to@domain.com')); ?>
                  </div>
                  <div class="form-group">
                      <label>Cc :</label>
                      <?php echo $this->Form->input('email_cc', array('div' => false, 'label' => false, 'class' => 'form-control','value'=>'','placeholder'=>'Ex: cc@domain.com,cc@domain.com')); ?>
                  </div>
                  <div class="form-group">
                      <label>Bcc :</label>
                      <?php echo $this->Form->input('email_bcc', array('div' => false, 'label' => false, 'class' => 'form-control','value'=>'','placeholder'=>'Ex: bcc@domain.com,bcc@domain.com')); ?>
                  </div>
                  <div class="form-group">
                      <label>Subject : </label>
                      <?php echo $this->Form->input('email_subject', array('div' => false, 'label' => false, 'class' => 'form-control')); ?>
                  </div>
                  <div class="form-group">
                      <label>Message : </label>
                      <?php echo $this->Form->hidden('email_from');
                       echo $this->Form->hidden('email_from_sign');
                       echo $this->Form->input('message' , array('div' => false, 'label' => false, 'class' => 'form-control ckeditor','id'=>'MessageContent','type'=>'textarea', 'rows'=>10, 'cols'=>10)); 
                      ?>
                  </div>
                   <?php 
                    if($email->attachment !='' && !empty($attachment = json_decode($email->attachment))):?>
                    <label>Attachment : </label>
                    <div class="form-group">
                      <?php 
                      $file_count = 1;
                      foreach($attachment as $filename):
                        $files = pathinfo($filename);?>
                        <div class="attachment-file-<?=$file_count?>">
                            <?php 
                            echo $this->Html->link('<i class="fa fa-file"></i> '. __($files['basename']),['controller'=>'emails','action' => 'download' , $files['basename']],['escape' => false]);
                            echo $this->Form->hidden('attachment[]', ['id'=>'file_name_hidden_'.$file_count , 'value'=>$filename]);
                            ?>
                            <a href="javascript:void(0)"><i class="fa fa-remove" data_id="<?=$file_count?>"></i></div>
                        </div>
                      <?php 
                      $file_count++;
                      endforeach;
                      ?>
                  </div>
                  <?php endif; ?>
                  <div class="ln_solid"></div>
                  <div class="form-group">
                        <button id="send" type="submit" class="btn btn-success">Send</button>
                        <a href="<?php echo $this->Url->build(['action' => 'index']); ?>" class="btn btn-primary">Cancel</a>
                  </div>
              </div>
              <?= $this->Form->end() ?>
          </section> 
      </div>
  </div>
</div>
</div>
</div>
</div>
<?php $this->start('scriptBottom'); ?>
<script type="text/javascript">
    jQuery(document).ready(function(){
        //Validate the sign up form
        $('#forward_email').validate({
            ignore: [],
            rules: {
                "email_to": {required: true},
                "email_subject": {required: true}
            },
             errorPlacement: function (error, element) {
               error.insertAfter(element);
            }
        });
    
        jQuery(document).on('click' , '.fa-remove' , function(){
            var data_id = jQuery(this).attr('data_id');
            jQuery(this).parents('div.attachment-file-'+data_id).remove();
        });
    });
</script>
<?php $this->end(); ?>