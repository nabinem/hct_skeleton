<?php use Cake\Routing\Router; ?>
<!-- page content -->
<div class="right_col index_page" role="main">
  <div class="">
    <div class="page-title">
      <div class="title_left">
        <h3>Email List<small> (Email Manager) </small></h3>
      </div>
      <div class="pull-right">
        <?= $this->Html->link('<i class="fa fa-envelope"></i> '. __('Compose'),['controller'=>'emails','action' => 'compose'],['class' => 'btn btn-primary btn-xs','escape' => false]) ?>
      </div>
    </div>
    <div class="clearfix"></div>
    <?= $this->Flash->render() ?>
    <?= $this->Flash->render('auth') ?>
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
             <div class="pull-left col-sm-8">
                <h2>Email(<?php echo isset($this->request->data['email_lable'])? ucfirst($this->request->data['email_lable']) :'Inbox'; ?>)</h2>
             </div>
            <div class="pull-right col-sm-4">
                <?= $this->Form->create(null , ['type'=>'GET']) ?>
                <div class="input-group">
                    <?=$this->Form->input('email_label', 
                    [
                      'label'=>false,
                       'templates' => [
                      'inputContainer' => '{{content}}'],
                      'type' => 'select',
                      'options' => $emailLabels, 
                      'class'=>'form-control',
                      'empty'=>'Select Email Label',
                      'width'=>'60%'
                    ]); ?>
                     <span class="input-group-btn">
                      <?= $this->Form->button('Search' , ['type'=>'Subject' , 'class'=>'btn btn-primary']); ?>
                      </span>
                    </div>
                <?= $this->Form->end() ?>
              </div>
            <div class="clearfix"></div>
          <div class="x_content">
              <div class="table-responsive">
                <table id="datatable" class="table table-striped table-bordered sticky_thead">
              <thead>
                <tr>
                    <th width="20%">From</th>
                    <th width="20%">To</th>
                    <th width="20%">Subject</th>
                    <th width="10%">Date Time</th>
                    <th width="25%">Action</th>
                </tr>
              </thead>
              <tbody>
                    <?php 
                    foreach ($emails as $email):  ?>
                    <tr>
                        <td width="20%"><?= $email->email_from ?></td>
                        <td width="20%">
                            <?= $email->email_to !=''?'Me':'' ?> <?= $email->email_cc !=''? ','.$email->email_cc:'' ?>
                        </td>
                        <td width="20%">
                            <?= $email->email_subject ?>
                        </td>
                        <td width="15%">
                            <?= $email->email_received_time->format('M d -  H:i A'); ?>
                        </td>
                        <td width="25%">
                          <?= $this->Html->link('<i class="fa fa-reply"></i> '. __('Reply'),['controller'=>'emails','action' => 'reply', $email->id],['class' => 'btn btn-primary btn-xs','escape' => false]) ?>

                          <?= $this->Html->link('<i class="fa fa-envelope-o"></i> '. __('View'), ['controller'=>'emails','action' => 'view',$email->id],['class' => 'btn btn-primary btn-xs','escape' => false]) ?>

                          <?= $this->Html->link('<i class="fa fa-share"></i> '. __('Forward'), ['controller'=>'emails','action' => 'forward', $email->id],['class' => 'btn btn-primary btn-xs','escape' => false]) ?>
                          <?= $this->Html->link('<i class="fa fa-trash-o"></i> '. __('Delete'), ['controller'=>'emails','action' => 'delete', $email->id , $email->email_msg_no], ['confirm' => __('Are you sure you want to delete email ?'),'class' => 'btn btn-danger btn-xs','escape' => false]) ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>                    
                </table>
              </div>
          </div>
        </div>
      </div>
    </div>
    <?php echo $this->element('pagination'); ?>
  </div>
</div>
<!-- /page content -->
<?php echo $this->Html->script(['jquery.floatThead.min'], ['block' => 'pluginJs']); ?>