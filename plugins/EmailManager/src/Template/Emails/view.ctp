<div class="right_col index_page" role="main">
    <div class="page-title">
      <div class="title_left">
         <h3>View Email<small> <a href="<?php echo $this->Url->build(['action' => 'index']); ?>"><a href="<?php echo $this->Url->build(['action' => 'index']); ?>">(Email Manager)</a></small></h3>
      </div>
      <div class="pull-right">
        <?= $this->Html->link('<i class="fa fa-envelope"></i> '. __('Email(Inbox)'),['controller'=>'emails','action' => 'index'],['class' => 'btn btn-primary btn-xs','escape' => false]) ?>
      </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>View</h2>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <div class="row">
                  <div class="form-group">
                      <label>Email From:</label>
                      <?= $email->email_from ?>
                  </div>
                  <div class="form-group">
                      <label>Email To:</label>
                      <?= $email->email_to ?>
                  </div>
                  <?php if($email->email_cc !='') {?>
                  <div class="form-group">
                      <label>Email Cc :</label>
                      <?= $email->email_cc ?>
                  </div>
                  <?php } ?>
                  <div class="form-group">
                      <label>Subject:</label>
                      <?= $email->email_subject ?>
                  </div>
                  <?php if($email->message !=''):?>
                   <div class="form-group">
                      <label>Message:</label>
                   </div>
                  <div class="form-group">
                      <?= base64_decode($email->message) ?>
                  </div>
                  <?php 
                  endif;
                  if($email->attachment !='' && !empty($attachment = json_decode($email->attachment))):?>
                   <div class="form-group">
                      <label>Attachment:</label>
                   </div>
                  <div class="form-group">
                      <?php foreach($attachment as $files):
                          $files = pathinfo($files);
                          echo "<div>".$this->Html->link('<i class="fa fa-file"></i> '. __($files['basename']),['controller'=>'emails','action' => 'download' , $files['basename']],['escape' => false])."</div>";
                      endforeach;
                      ?>
                  </div>
                  <?php endif; ?>
              </div>
          </div>
      </div>
  </div>
</div>
</div>

    


