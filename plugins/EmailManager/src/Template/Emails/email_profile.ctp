 <div class="right_col index_page" role="main">
    <div class="">
      <div class="page-title">
        <div class="title_left">
           <h3>Email Profile<small> <a href="<?php echo $this->Url->build(['action' => 'index']); ?>"><a href="<?php echo $this->Url->build(['action' => 'index']); ?>">(Email Manager)</a></small></h3>
        </div>
        <div class="pull-right">
          <?= $this->Html->link('<i class="fa fa-user"></i> '. __('Users'),['plugin'=>false,'controller'=>'users','action' => 'index'],['class' => 'btn btn-primary btn-xs','escape' => false]) ?>
        </div>
      </div>
      <div class="clearfix"></div>
      <?= $this->Flash->render() ?>
      <?= $this->Flash->render('auth') ?>
      <?= $this->Flash->render('ckeditor') ?>
      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>Email Profile</h2>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              <?php 
              echo $this->Form->create($emailProfile, ['class' => 'form-horizontal form-label-left', 
                  'templates' => ['inputContainer' => '{{content}}'], 'id' => 'email_edit_form']) ?>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="user_email">Email <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <?php 
                    echo $this->Form->input('user_email', [
                        'div' => false, 'label' => false, 'class' => 'form-control col-md-7 col-xs-12', 'required' => true]); ?>
                </div>
              </div>
              <div class="item form-group">
                <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email_from_sign">Email From Sign <span class="required">*</span>
                </label>
                <div class="col-md-6 col-sm-6 col-xs-12">
                    <?php 
                    echo $this->Form->hidden('user_id' ,['id'=>'user_id']);
                    echo $this->Form->input('email_from_sign', [
                        'div' => false, 'label' => false, 'class' => 'form-control col-md-7 col-xs-12', 'required' => true]); ?>
                </div>
              </div>
                <div class="item form-group">
                  <label class="control-label col-md-3 col-sm-3 col-xs-12" for="user_password">Password <span class="required">*</span>
                  </label>
                  <div class="col-md-6 col-sm-6 col-xs-12">
                      <?php 
                      if(isset($emailProfile->user_password)):
                          echo $this->Form->input('user_password', [
                          'type'=>'password','div' => false, 'label' => false, 'class' => 'form-control col-md-7 col-xs-12','value'=>'' , 'placeholder'=>'******']);
                      else: 
                         echo $this->Form->input('user_password', [
                          'type'=>'password','div' => false, 'label' => false, 'class' => 'form-control col-md-7 col-xs-12', 'required' => true]); 
                      endif;
                      ?>
                   </div>
                </div>
                <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="status">Status<span class="required">*</span></label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <?php 
                        $attributes = array('value' => isset($emailProfile->status) ? $emailProfile->status: '1');
                        echo $this->Form->radio('status', array('1'=>'Active' , '0'=>'Inactive') , $attributes , ['required' => true]);
                        ?>
                    </div>
                </div>
                <div class="item form-group">
                    <label  class="control-label col-md-3 col-sm-3 col-xs-12" for="email_signature">Email Signature</label>
                    <div class="col-md-9 col-sm-9 col-xs-12">
                        <?php echo $this->Form->input('email_signature' , array('div' => false, 'label' => false, 'class' => 'form-control ckeditor','id'=>'MessageContent','type'=>'textarea', 'rows'=>10, 'cols'=>10)); ?>
                    </div>
                </div>
                <div class="ln_solid"></div>
                <div class="form-group">
                  <div class="col-md-6 col-md-offset-3">
                      <?php echo $this->Form->button('Submit' , array('type'=>'submit','div' => false, 'class' => 'btn btn-success')); ?>
                      <?= $this->Html->link('Cancel',['plugin'=>false,'controller'=>'users','action' => 'index'],['class' => 'btn btn-primary','escape' => false]) ?>
                  </div>
                </div>
            <?= $this->Form->end() ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<?php $this->start('scriptBottom'); ?>
<script type="text/javascript">
    jQuery(document).ready(function(){
        //Validate the sign up form
        $('#email_edit_form').validate({
            rules: {
                "user_email": {required: true},
                "email_from_sign": {required: true},
                "user_password": {
                    required: {
                        depends: function(element) {
                            var user_id = $("#user_id").val();
                            return (user_id =='' && element.value == '');
                        }
                    }
                }
            }
        });
    });
</script>
<?php $this->end(); ?>