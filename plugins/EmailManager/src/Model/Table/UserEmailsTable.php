<?php 
namespace EmailManager\Model\Table;

use Cake\ORM\Table;

class UserEmailsTable extends Table
{
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->table('user_emails');
        $this->displayField('id');
        $this->primaryKey('id');
        
        

        $this->addAssociations([
           'belongsTo' => [
               'Users' => ['className' => 'App\Model\Table\UsersTable' , 'foreignKey'=>'user_id '],
               'UserEmailProfiles' => ['className' => 'EmailManager.UserEmailProfiles' , 'foreignKey'=>'user_email_profile_id ']
           ],
          
        ]);


    }
}