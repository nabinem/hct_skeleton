<?php
namespace EmailManager\Controller\Component;

use Cake\Controller\Component;
use Cake\ORM\TableRegistry;
use Cake\Utility\Security;
use Cake\I18n\Time;
use Cake\Mailer\Email;

class ImapComponent extends Component
{

	// Declare imap object
	public $imap;

	/*
     * @author: HCT, USA
     * @created: 11 June, 2017
     * @createdBy: Mudit Moha Tyagi
     * @modified: 13 June, 2017
     * @purpose: connect with email server
    */ 
    public function imapConnection($user_id = '')
    {
    	$this->UserEmails = TableRegistry::get('UserEmails');  
        $this->UserEmailProfiles = TableRegistry::get('UserEmailProfiles'); 
        $emailSetting = $this->UserEmailProfiles->find()
                ->where(['user_id'=>$user_id])
                ->first();
       
        if(!empty($emailSetting))
        {

            $imap_path = '{mail.healthcaretouch.com:993/imap/ssl/novalidate-cert}';
            $username = $emailSetting->user_email;
            $password = Security::decrypt(base64_decode($emailSetting->user_password), CIPHER_KEY);  
            // try to connect
            try 
            {
                return $this->imap = imap_open($imap_path,$username,$password); 
            } 
            catch (Exception $ex) 
            {

                return false;
            }
            return false;
        }
    }

     /*
     * @author: HCT, USA
     * @created: 11 June, 2017
     * @createdBy: Mudit Moha Tyagi
     * @modified: 13 June, 2017
     * @purpose: download email html content as html and all attachments at given path
    */ 
    public function getEmails($user_id = '' , $emailSetting = array())
    {
    	ini_set('memory_limit', '1024M');   
    	ini_set('max_execution_time', -1); 
        $this->imap = $this->imapConnection($user_id);   
        $imap_path = '{mail.healthcaretouch.com:993/imap/ssl/novalidate-cert}';            
        $emailLabel = imap_list($this->imap, $imap_path, '*');
        if(!empty($emailLabel))
        {
            foreach ($emailLabel as $label)
            {
                // search and get unseen emails, function will return email ids
                $tempLable = explode("}" , $label);
                $labelName = end($tempLable);

                $userEmails = $this->UserEmails->find()
                        ->where(['user_id'=>$user_id , 'email_label'=>$labelName])
                        ->count();

                //imap_reopen($this->imap, $label);
                imap_reopen($this->imap, $label);
                if($userEmails > 0)
                {
                    $excludedLavels = array('inbox.trash','trash','inbox.sent','sent');
                    if(in_array(strtolower($labelName) , $excludedLavels))
                    {
                        $emails = imap_search($this->imap,'ALL');
                    }
                    else
                    {
                        $emails = imap_search($this->imap,'UNSEEN');
                    }
                }
                else
                {
                    $emails = imap_search($this->imap, 'ALL');
                }
                if(!empty($emails))
                {
                    //put the newest emails on top
                    rsort($emails);
                    $time = new Time();
                    $current_date = $time->format('Y-m-d H:i:s'); 
                    foreach($emails as $email_number)
                    {
                        $overview = imap_fetch_overview($this->imap, $email_number, 0);
                        $header = imap_headerinfo($this->imap, $email_number); 
                        //store email details to save in database
                        $emailExistenceCheck = $this->UserEmails->find()
                                ->where(['user_id'=>$user_id , 'email_uid'=>$overview[0]->uid , 'email_label'=>$labelName , 'email_from'=>$this->concatEmailAddress($header->from)])
                                ->count();

                        if($emailExistenceCheck == '0')
                        {
                            $structure = imap_fetchstructure($this->imap, $email_number);
                            $encoding = $structure->encoding;
                            $message = imap_fetchbody($this->imap, $email_number, 1.2);
                            if($message == "")
                            {
                                $message = imap_body($this->imap, $email_number);
                                if($encoding == 3)
                                {
                                    $message = base64_decode($message);
                                }
                                else if($encoding == 4)
                                {
                                    $message = quoted_printable_decode($message);
                                }
                            }

                            $attachments = array();
                            $structure = imap_fetchstructure($this->imap, $email_number);
                            if(property_exists($structure, 'parts'))
                            {
                                $flag = 1;
                                $flattened_parts = $this->flattenParts($structure->parts);
                                foreach($flattened_parts as $part_number => $part)
                                {
                                    switch($part->type)
                                    {
                                        case 0:
                                            if((isset($part->subtype)=='PLAIN') && (isset($part->disposition)=='ATTACHMENT'))
                                            {
                                                  $part_number = 1.1;
                                            }
                                            else if((isset($part->subtype)=='HTML') && (isset($part->disposition)=='ATTACHMENT'))
                                            {
                                                  $part_number = 1.2;
                                            }
                                            else if(isset($part->subtype)=='HTML')
                                            {
                                                 $part_number = $part_number;
                                            }
                                            else
                                            {
                                                 $part_number = $part_number;
                                            } 
                                            $message = $this->getParts($this->imap, $email_number, $part_number, $part->encoding);
                                       break;
                                       case 1: // multi-part headers, can ignore
                                       case 2: // attached message headers, can ignore
                                       case 3: // application
                                       case 4: // audio
                                       case 5: // image
                                       case 6: // video
                                       case 7: // other
                                       break;

                                    }
                                    if(isset($part->disposition) || isset($part->subtype) == "OCTET-STREAM")
                                    {

                                        $filename = $this->getFilenameFromParts($part);
                                        if($filename)
                                        {   
                                            // save attachemnt
                                            $attchmnt = $this->getParts($this->imap, $email_number, $part_number, $part->encoding);
                                            $file_info = pathinfo($filename);
                                            $includeFiels = array('txt','rtf','png','jpeg','jpg','gif','tiff','pdf','doc','docx','xls','xlsx','ppt','odt','html','zip');
                                            if(in_array(strtolower($file_info['extension']) , $includeFiels))
                                            {
                                                $attachment_dir = ATTACHMENT;
                                                if(!is_dir($attachment_dir)) mkdir($attachment_dir, '0777' , true);
                                                //$attached_filename = $overview[0]->uid.'_'.mt_rand(1000,9999).time().rand(100,999).'.'.$file_info['extension'];
                                                $attached_filename = $this->clean($file_info['filename']).'.'.$file_info['extension'];
                                                $this->saveAttachment($attchmnt, $attached_filename, $attachment_dir);
                                                $attachments[] = $attachment_dir.$attached_filename;
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                $encoding = $structure->encoding;
                                $message = imap_fetchbody($this->imap, $email_number, 1.2);
                                if($message == '')
                                {
                                    $message = imap_body($this->imap, $email_number);
                                    if($encoding == 3)
                                    {
                                        $message = base64_decode($message);
                                    }
                                    else if($encoding == 4)
                                    {
                                        $message = quoted_printable_decode($message);
                                    }
                                }   
                            }
                            $time = new Time($header->MailDate);
                            $mail_date = $time->format('Y-m-d H:i:s'); 
                            $saveData = $this->UserEmails->newEntity();                
                            $saveData->email_msg_no = $overview[0]->msgno;
                            $saveData->email_uid = $overview[0]->uid;
                            $saveData->user_id = $user_id;
                            $saveData->user_email_profile_id = $emailSetting->id;
                            $saveData->email_from = $this->concatEmailAddress($header->from);
                            $saveData->email_to = isset($header->to)?$this->concatEmailAddress($header->to):NULL;
                            $saveData->email_cc = isset($header->cc) ? $this->concatEmailAddress($header->cc) : NULL;
                            $saveData->email_subject = isset($header->subject) ? $header->subject : "";
                            $saveData->email_received_time = $mail_date;
                            $saveData->sync_date_time = $current_date; 
                            $saveData->email_label = $labelName; 
                            if($overview[0]->seen == '1')
                            {
                                // Setting flag from un-seen email to seen on emails id.
                                $saveData->status = '1';
                                imap_setflag_full($this->imap,$email_number, "\\Seen \\Flagged");
                            }
                            // check message body
                            if(!$message == '')
                            {
                                //remove special char
                                $message = preg_replace(array('/[^\r\n\t\x20-\x7E\xA0-\xFF]*/'), '', $message);
                                $message = str_replace(chr(194),"",$message); 
                                $saveData->message = base64_encode($message);
                            }

                            // json encode all attachment links if found
                            if(!empty($attachments)) 
                            {
                                $saveData->attachment = json_encode($attachments);
                            }
                            // save Useremail in table
                            if($this->UserEmails->save($saveData))
                            {
                               // add log if required
                            }
                        }
                    } 
                }
            }
            imap_close($this->imap);
        }

	}


    /*
     * @author: HCT, USA
     * @created: 11 June, 2017
     * @createdBy: Mudit Moha Tyagi
     * @modified: 13 June, 2017
     * @purpose: get parts(contents)
    */ 
    private function getParts($connection, $message_number, $part_number, $encoding)
    {
    	$data = imap_fetchbody($connection, $message_number, $part_number);
    	switch($encoding)
    	{
    	    case 0: return $data; // 7BIT
    	    case 1: return $data; // 8BIT
    	    case 2: return $data; // BINARY
    	    case 3: return base64_decode($data); // BASE64
    	    case 4: return quoted_printable_decode($data); // QUOTED_PRINTABLE
    	    case 5: return $data; // OTHER
    	}
    }
    
    /*
     * @author: HCT, USA
     * @created: 11 June, 2017
     * @createdBy: Mudit Moha Tyagi
     * @modified: 13 June, 2017
     * @purpose: clean string remove spaces and special char
    */ 
    public function clean($string) 
    {
        $string = str_replace(' ', '_', $string); // Replaces all spaces.
        return preg_replace('/[^A-Za-z0-9\-_]/', '', $string); // Removes special chars.
    }

    
    /*
     * @author: HCT, USA
     * @created: 12 June, 2017
     * @createdBy: Mudit Moha Tyagi
     * @modified: 15 June, 2017
     * @purpose: save attachment
    */ 
    private function saveAttachment( $content , $filename , $directory_path )
    {
    	$file = fopen($directory_path.DS.$filename, 'w');
    	fwrite($file, $content);
    	fclose($file);
    } 
    
    /*
     * @author: HCT, USA
     * @created: 13 June, 2017
     * @createdBy: Mudit Moha Tyagi
     * @modified: 13 June, 2017
     * @purpose: get filename from parts
    */ 
    private function getFilenameFromParts($part)
    {
        $filename = '';
        if(!isset($part->id) && $part->ifdparameters && $part->ifdparameters && strtolower($part->disposition)=='attachment')
    	{
           
    	    foreach($part->dparameters as $object)
    	    {
    		if(strtolower($object->attribute) == 'filename')
    		{
    		    $filename = $object->value;
    		}
    	    }
    	}
    	if(!isset($part->id) && !$filename && $part->ifparameters && $part->ifdparameters && strtolower($part->disposition)=='attachment')
    	{
            
    	    foreach($part->parameters as $object)
    	    {
    		if(strtolower($object->attribute) == 'name')
    		{
    		    $filename = $object->value;
    		}
    	    }
    	}
    	return $filename;
    }

    
    /*
     * @author: HCT, USA
     * @created: 14 June, 2017
     * @createdBy: Mudit Moha Tyagi
     * @modified: 14 June, 2017
     * @purpose: get flatter parts
    */ 
    private function flattenParts($message_parts, $flattened_parts = array(), $prefix = '', $index = 1, $full_prefix = true)
    {
	
        foreach($message_parts as $part)
    	{
    	    $flattened_parts[$prefix.$index] = $part;
    	    if(isset($part->parts))
    	    {
    		if($part->type == 2)
    		{
    		    $flattened_parts = $this->flattenParts($part->parts, $flattened_parts, $prefix.$index.'.', 0, false);
    		}
    		elseif($full_prefix)
    		{
    		    $flattened_parts = $this->flattenParts($part->parts, $flattened_parts, $prefix.$index.'.');
    		}
    		else
    		{
    		    $flattened_parts = $this->flattenParts($part->parts, $flattened_parts, $prefix);
    		}
    		unset($flattened_parts[$prefix.$index]->parts);
    	    }
    	    $index++;
    	}
    	return $flattened_parts;
    }


    /*
     * @author: HCT, USA
     * @created: 14 June, 2017
     * @createdBy: Mudit Moha Tyagi
     * @modified: 14 June, 2017
     * @purpose: concat email and email sign
    */ 
    private function concatEmailAddress($data)
    {
        $result = array();
        foreach($data as $key=>$value){
            $result[] = $value->mailbox . '@' . $value->host;
        }
        return implode(' , ',$result);
    } 

    

    /*
     * @author: HCT, USA
     * @created: 21 June, 2017
     * @createdBy: Mudit Moha Tyagi
     * @modified: 21 June, 2017
     * @purpose: download attachment
     * @params required filename
    */ 
    public function download($file=null) 
    { 
        $this->autoRender = false;
        $filePath = ATTACHMENT. $file;
        $this->response->file($filePath ,array('download'=> true, 'name'=> $file));
    }


    /*
     * @author: HCT, USA
     * @created: 14 June, 2017
     * @createdBy: Mudit Moha Tyagi
     * @modified: 17 June, 2017
     * @purpose: send email/to/cc/bcc 
     * @params required array()
    */ 
    public function sendMail($data = array())
    {

        $email = new Email('gmail');
        $email->to(array_map('trim',explode(",",$data['email_to']))); 
        if(!empty($data['email_cc']))
        {
            $email->cc(array_map('trim',explode(",",$data['email_cc']))); 
        }
        if(!empty($data['email_bcc']))
        {
            $email->bcc(array_map('trim',explode(",",$data['email_bcc']))); 
        } 
        $email_from = $data['email_from'];    
        $email_from_sign = $data['email_from_sign'];
        $email_subject = $data['email_subject'];
        $email->emailFormat('html');
        $email->from(array($email_from => $email_from_sign));
        $email->subject($email_subject);
        
        if(isset($data['attachment']) && !empty($data['attachment']))	
        {
           $email->attachments($data['attachment']);
        }
        try 
        {
           $email->send($data['message']);
           $return = true;
        } catch (Exception $e) {
           $return = false;
        }
        return true;
    }
    
   
}
