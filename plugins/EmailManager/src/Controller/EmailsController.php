<?php
namespace EmailManager\Controller;

use EmailManager\Controller\AppController;
use Cake\Event\Event;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;
use Cake\Utility\Security;
use Cake\Network\Exception\NotFoundException;
use Cake\Controller\Component;

class EmailsController extends AppController
{
    
    /*
     * @purpose: before filter
    */ 
    public function beforeFilter(Event $event)
    {
        $this->Auth->allow(['compose','index', 'view', 'reply','forward','delete','emailProfile','download']);
        $this->UserEmails = TableRegistry::get('UserEmails');  
        $this->UserEmailProfiles = TableRegistry::get('UserEmailProfiles'); 
        $this->LoadComponent('EmailManager.Imap');
    } 
    
   
    /*
     * @author: HCT, USA
     * @created: 21 June, 2017
     * @createdBy: Mudit Moha Tyagi
     * @modified: 21 June, 2017
     * @purpose: add/update user email profile
    */ 
    public function emailProfile($user_id = '')
    {
        if(empty($user_id)) throw new NotFoundException(__('Invalid Request'));
        $emailProfile = $this->UserEmailProfiles->find()->where(['user_id'=>$user_id])->first();
        if(empty($emailProfile)) $emailProfile = $this->UserEmailProfiles->newEntity();
        
        if ($this->request->is(array('post','put')) && !empty($this->request->data)) 
        {
            if($this->request->data['user_id'] == '')
            {
                $this->request->data['user_id'] = $user_id;
                $success_msg = 'Email profile has been added successfully.';
            }
            else
            {
                $success_msg = 'Email profile has been updated successfully.';
            }
            if($this->request->data['user_password'] != '')
            {
                $emailProfile->user_password = base64_encode(Security::encrypt($this->request->data['user_password'], CIPHER_KEY));  
                
            }
            unset($this->request->data['user_password']);
            $emailProfile = $this->UserEmailProfiles->patchEntity($emailProfile , $this->request->data);
            if($this->UserEmailProfiles->save($emailProfile))
            {
                $this->Flash->success(__($success_msg));
                return $this->redirect(['plugin'=>false,'controller'=>'users','action' => 'index']);
            }
            else
            {
                $this->Flash->error(__('Email is not sent successfully. Please try again.'));
            }
        }
        $this->set(compact('emailProfile'));
    }


    /*
     * @author: HCT, USA
     * @created: 14 June, 2017
     * @createdBy: Mudit Moha Tyagi
     * @modified: 14 June, 2017
     * @purpose: emails listing
    */ 
    public function index()
    {

        $user_id = $this->Auth->user('id');
        $this->UserEmails = TableRegistry::get('UserEmails');  
        $this->UserEmailProfiles = TableRegistry::get('UserEmailProfiles'); 
        $emailSetting = $this->UserEmailProfiles->find()
                ->where(['user_id'=>$user_id])
                ->first();
        
        if(!empty($emailSetting))
        {
            $current_time = time();
            if($current_time - $emailSetting->recent_connection >300) 
            {
                $this->Imap->getEmails($user_id ,$emailSetting);
                $emailSetting->recent_connection = $current_time;
                $this->UserEmailProfiles->save($emailSetting);
            }
        }
        else
        {
            $this->Flash->error(__('Your email setting is not found.'));
            return $this->redirect(['plugin'=>false,'controller'=>'dashboards','action' => 'index']);    
        }

        if(isset($this->request->query['email_label']) && $this->request->query['email_label'] !='')
        {
            $conditons['email_label'] = $this->request->query['email_label'];
            $this->request->data['email_label'] = $this->request->query['email_label'];
        }
        else
        {
            $this->request->data['email_label'] = 'INBOX';
            $conditons['email_label'] = $this->request->data['email_label'];
        }
        $conditons['user_id'] = $user_id;
        $this->paginate = ['sortWhitelist' => ['email_from']];  
        $query = $this->UserEmails->find()
                ->where($conditons)
                ->order(['email_received_time'=>'DESC']);
        
        $emailLabels = $this->UserEmails->find('list',['keyField'=>'email_label' , 'valueField'=>function ($data) {
            $a = $data->email_label; 
            $a = explode("." , $a);
            return strtoupper(end($a));
        }])->group('email_label')->toArray();
        
        $emails = $this->paginate($query);
        $this->set(compact('emails','emailLabels'));
    }  
    
    /*
     * @author: HCT, USA
     * @created: 16 June, 2017
     * @createdBy: Mudit Moha Tyagi
     * @modified: 16 June, 2017
     * @purpose: view email by email id
    */ 
    public function view($id = null)
    {
        $email = $this->UserEmails->get($id);
        $this->set(compact('email'));
    }

    /*
     * @author: HCT, USA
     * @created: 17 June, 2017
     * @createdBy: Mudit Moha Tyagi
     * @modified: 17 June, 2017
     * @purpose: delete email by primary key
    */ 
    public function delete($id = '' , $msgno = '')
    {
        $entity = $this->UserEmails->get($id);
        if($this->UserEmails->delete($entity))
        {
            $this->imap = $this->Imap->imapConnection();
            if($this->imap) imap_delete($this->imap, $msgno);
            $this->Flash->success(__('Email has been deleted successfully.'));
            return $this->redirect(['action' => 'index']);
        }
        else
        {
            $this->Flash->error(__('Email is not deleted successfully. Please try again.'));
        }
    }
    

    /*
     * @author: HCT, USA
     * @created: 17 June, 2017
     * @createdBy: Mudit Moha Tyagi
     * @modified: 17 June, 2017
     * @purpose: Forward email with to/cc/bcc
    */ 
    public function forward($id = null)
    {
        $email = $this->UserEmails->find()->where(['id'=>$id])->first();
        $signature = $this->UserEmailProfiles->find()->select(['email_from_sign','email_signature'])->where(['id'=>$email['user_email_profile_id']])->hydrate(false)->first();
        $email->email_from_sign = $signature['email_from_sign'];
        $email->message = base64_decode($email->message).$signature['email_signature'];
        if ($this->request->is(array('post','put')) && !empty($this->request->data)) {
            if($this->Imap->sendMail($this->request->data))
            {
                $this->Flash->success(__('Email has been sent successfully.'));
                return $this->redirect(['action' => 'index']);
            }
            else
            {
                $this->Flash->error(__('Email is not sent successfully. Please try again.'));
            }
            
        }
        $this->set(compact('email','signature'));
    }


    /*
     * @author: HCT, USA
     * @created: 17 June, 2017
     * @createdBy: Mudit Moha Tyagi
     * @modified: 17 June, 2017
     * @purpose: send email
    */ 
    public function compose()
    {
        $user_id = $this->Auth->user('id');
        $email = $this->UserEmails->newEntity();
        $userEmailProfile = $this->UserEmailProfiles->find()->select(['user_email','email_from_sign','email_signature'])->where(['user_id'=>$user_id])->hydrate(false)->first();
        $email->email_from = $userEmailProfile['user_email'];
        $email->email_from_sign = $userEmailProfile['email_from_sign'];
        $email->message = $userEmailProfile['email_signature'];
        if ($this->request->is(array('post','put')) && !empty($this->request->data)) {
            if($this->sendMail($this->request->data))
            {
                $this->Flash->success(__('Email has been sent successfully.'));
                return $this->redirect(['action' => 'index']);
            }
            else
            {
                $this->Flash->error(__('Email is not sent successfully. Please try again.'));
            }
            
        }
        $this->set(compact('email'));
    }



     /*
     * @author: HCT, USA
     * @created: 16 June, 2017
     * @createdBy: Mudit Moha Tyagi
     * @modified: 16 June, 2017
     * @purpose: Reply on email 
    */ 
    public function reply($id = null)
    {

        $email = $this->UserEmails->find()->where(['id'=>$id])->first();
        $this->Users = TableRegistry::get('Users');
        $signature = $this->UserEmailProfiles->find()->select(['email_from_sign','email_signature'])->where(['id'=>$email['user_email_profile_id']])->hydrate(false)->first();
        $email->email_from_sign = $signature['email_from_sign'];
        $email->message = $signature['email_signature'];
        if ($this->request->is(array('post','put')) && !empty($this->request->data)) {
            if($this->Imap->sendMail($this->request->data))
            {
                $this->Flash->success(__('Email has been sent successfully.'));
                return $this->redirect(['action' => 'index']);
            }
            else
            {
                $this->Flash->error(__('Email is not sent successfully. Please try again.'));
            }
            
        }
        $this->set(compact('email','signature'));
    }
    
    
    
}

