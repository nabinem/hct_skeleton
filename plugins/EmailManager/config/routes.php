<?php
use Cake\Routing\Route\DashedRoute;
use Cake\Routing\Router;

Router::plugin(
    'EmailManager',
    ['path' => '/email-manager'],
    function ($routes) {
        $routes->fallbacks('DashedRoute');
    }
);
