<!-- page content -->
        <div class="right_col index_page" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Affiliates List<small> (list of all affiliates) </small></h3>
              </div>
            </div>

            <div class="clearfix"></div>
            <?= $this->Flash->render() ?>
            <?= $this->Flash->render('auth') ?>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Affiliates</h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                      <div class="table-responsive">
                        <table id="datatable" class="table table-striped table-bordered sticky_thead">
                      <thead>
                        <tr>
                          <th><?php echo $this->Paginator->sort('firstname', 'First Name'); ?></th>
                          <th><?php echo $this->Paginator->sort('lastname', 'Last Name'); ?></th>
                          <th><?php echo $this->Paginator->sort('username'); ?></th>
                          <th><?php echo $this->Paginator->sort('email'); ?></th>
                          <th><?php echo $this->Paginator->sort('active'); ?></th>
                          <th><?php echo $this->Paginator->sort('created'); ?></th>
                          <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                          <?php foreach ($affiliates as $affiliate): ?>
                                <tr>
                                    <td><?= h($affiliate->firstname) ?></td>
                                    <td><?= h($affiliate->lastname) ?></td>
                                    <td><?= h($affiliate->username) ?></td>
                                    <td><?= h($affiliate->email) ?></td>
                                    <td align="center">
                                        <?php if ($affiliate->active): ?>
                                            <a href="<?php echo $this->Url->build(['action' => 'toggleStatus', $affiliate->id, 'inactive']); ?>" data-toggle="tooltip" title="Click to deactivate Affiliate Account.">
                                                <i class="fa fa-check text-success"></i>
                                            </a>
                                        <?php else: ?>
                                            <a href="<?php echo $this->Url->build(['action' => 'toggleStatus', $affiliate->id, 'active']); ?>" data-toggle="tooltip" title=" Click to activate Affiliate Account">
                                                <i class="fa fa-times text-danger"></i>
                                            </a>
                                        <?php endif; ?>
                                    </td>
                                    <td><?= h($affiliate->created) ?></td>
                                    <td class="actions">
                                        <a class="btn btn-dark btn-xs" href="<?php echo $this->Url->build(['action' => 'view', $affiliate->id]); ?>" data-toggle="tooltip" title="Click to View Affiliate details.">
                                                <i class="fa fa-eye"></i> View
                                            </a>
                                        <?= $this->Form->postLink('<i class="fa fa-trash-o"></i> '. __('Delete'), ['action' => 'delete', $affiliate->id],
                                                ['confirm' => __('Are you sure you want to delete Affiliate  {0}?', $affiliate->firstname),
                                                    'class' => 'btn btn-danger btn-xs', 'escape' => false]) ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                         
                      </tbody>
                    </table>
                      </div>
                  </div>
                </div>
              </div>
            </div>
            <?php echo $this->element('pagination'); ?>
          </div>
        </div>
        <!-- /page content -->
        <?php echo $this->Html->script(['jquery.floatThead.min'], ['block' => 'pluginJs']); ?>