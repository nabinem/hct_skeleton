<?php echo $this->Form->create(null, [
    'url' => ['controller' => 'Appointments', 'action' => 'index'],
    'type' => 'get', 'class' => 'form-inline', 'templates' => ['inputContainer' => '{{content}}']
]);
?>
<input type="hidden" name="search_mode" value="appointment_search">
<div class="form-group">
    <label> Doctor</label>
    <?php echo $this->Form->input('doctor_id', [
        'div' => false, 'label' => false, 'class' => 'form-control',
        'multiple' => true, 'id' => 'doc_idsearchdrp',
        'required' => false, 'options' => $doctors, 'value' => $this->request->query('doctor_id')]); ?>
</div>
<div class="form-group date">
   <input type="text" name="appointment_from" class="form-control" placeholder="Appointment From" id="appointment_from" style="width: 140px" 
        value="<?php echo !empty($this->request->query('appointment_from')) ? $this->request->query('appointment_from') : ''; ?>">
</div>
<div class="form-group date">
  <input type="text" name="appointment_to" class="form-control" placeholder="Appointment To" id="appointment_to" style="width: 140px"
         value="<?php echo !empty($this->request->query('appointment_to')) ? $this->request->query('appointment_to') : ''; ?>">
</div>
<button type="submit" class="btn btn-primary">Search</button>
 <?php if (isset($searchActive) && $searchActive === true): ?>
  <a href='<?php echo $this->Url->build(['action' => 'index', '?' => ['clear_search_filter' => true]]); ?>' class="btn btn-danger">
     Clear Filters & Reload
 </a>
 <?php endif; ?>
<?php echo $this->Form->end(); ?>
<?php echo $this->Html->css(['bootstrap-multiselect', '../vendors/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min'],['block'=>'pluginCss']); ?> 
<?php echo $this->Html->script(['bootstrap-multiselect.min', '../vendors/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min'], ['block' => 'pluginJs']); ?>
<?php $this->append('scriptBottom'); ?>
    <script type="text/javascript">
        jQuery(document).ready(function(){
            $('#appointment_from, #appointment_to').datetimepicker({format: 'MM/DD/YYYY', useCurrent: false});
            $('#doc_idsearchdrp').multiselect({
                enableFiltering: true,
                enableClickableOptGroups: true,
                enableCaseInsensitiveFiltering: true,
                nonSelectedText: 'None selected',
                nSelectedText: ' selected',
                allSelectedText: 'All selected',
                includeSelectAllOption: true,
                selectAllText: 'Select All'
            });
        });
    </script>
<?php $this->end(); ?>