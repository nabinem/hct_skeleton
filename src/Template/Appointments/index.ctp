<?php use Cake\Core\Configure; ?>
<!-- page content -->
        <div class="right_col index_page" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
                <h3>Appointments</h3>
              </div>
            </div>
            <div class="clearfix"></div>
            <?= $this->Flash->render() ?>
            <?= $this->Flash->render('auth') ?>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_content">
                         <div class="btn-group">
                            <button type="button" class="btn btn-dark dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                              <?php $allLeadStatus = Configure::read('allLeadStatus');
                                    $filterText = 'Filter by';
                                    if (is_numeric($this->request->query('status'))
                                            && isset($allLeadStatus[$this->request->query('status')])){
                                        $filterText = $allLeadStatus[$this->request->query('status')];
                                    } elseif(!empty($this->request->query['favorite']) && $this->request->query['favorite'] == 'starred'){
                                        $filterText = 'Starred';
                                    } elseif(!empty($this->request->query['favorite']) && $this->request->query['favorite'] == 'unstarred'){
                                        $filterText = 'Unstarred';
                                    }
                                    echo $filterText;
                                    ?>
                              <span class="caret"></span>
                              <span class="sr-only">Toggle Dropdown</span>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                                <li><a href="<?php echo $this->Url->build(['action' => 'index', '?' => ['clear_search_filter' => 1]]); ?>">All</a></li>
                                <?php foreach ($apptStatus as $val): 
                                    
                                    ?> 
                                    <li><a href="<?php echo $this->Url->build(['action' => 'index',  "?" => ["status" => $val]]); ?>">
                                        <?php echo $allLeadStatus[$val]; ?>
                                        </a></li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                      <div class="btn-group">
                           <?php echo $this->element('../Appointments/appointment_search'); ?>
                      </div>
                  <?php if ($this->AuthUser->hasRoles([ROLE_ADMIN, ROLE_STAFF])): ?>
                   <!--Export button starts-->
                   <div class="btn-group pull-right">
                       <a href="<?php echo $this->Url->build(['action' => 'exportExcel', '?' => $this->request->query]); ?>" target="_blank" class="btn btn-warning">
                        Export To Excel  <i class="fa fa-share"></i> 
                    </a>
                  </div>
                   <!--Export button- ends-->
                  <?php endif; ?>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                      <div class="table-responsive">
                    <table id="appt_list" class="table table-striped table-bordered table-hover sticky_thead">
                      <thead>
                        <tr>
                          <th><?php echo $this->Paginator->sort('id', 'ID'); ?></th>
                          <th><?php echo $this->Paginator->sort('Doctors.Users.first_name', 'Doctor Name'); ?></th>
                          <th><?php echo $this->Paginator->sort('first_name', 'Patient Name'); ?></th>
                          <th>Notes by Eligibility agent</th>
                          <th><?php echo $this->Paginator->sort('appointment', 'Appointment Date'); ?></th>
                          <th><?php echo $this->Paginator->sort('appointment', 'Appointment Time'); ?></th>
                          <th>Status</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php foreach ($leads as $lead): 
                            $link =  $this->Url->build(['controller' => 'Leads', 'action' => 'view', $lead->id]);?>
                          <tr data-lead_id="<?php echo $lead->id; ?>">
                              <td>
                                  <a href="<?=$link?>"  data-toggle="tooltip" title="Click to view more">
                                      <?= '#'.$lead->id; ?>
                                  </a>
                              </td>
                              <td>
                                  <a href="<?=$link?>"  data-toggle="tooltip" title="Click to view more">
                                       <?php if (!empty($lead->doctor->user)){
                                           echo h($lead->doctor->user->full_name);
                                       } ?>
                                  </a>
                                </td>
                                <td><a href="<?=$link?>"  data-toggle="tooltip" title="Click to view more">
                                         <?php echo h($lead->full_name); ?>
                                    </a>
                                 </td>
                                 <td><a href="<?=$link?>"  data-toggle="tooltip" title="Click to view more">
                                        <?php if (!empty($lead->lead_latest_status)){
                                        echo $lead->leadLatestApptStat(['apptStatus' => $apptStatus, 'withNote' => true]); 
                                        } ?>
                                     </a>
                              </td>
                              <td><a href="<?=$link?>"  data-toggle="tooltip" title="Click to view more">
                                       <?php echo $lead->appointment_date; ?>
                                  </a>
                              </td>
                              <td><a href="<?=$link?>"  data-toggle="tooltip" title="Click to view more">
                                       <?php echo $lead->appointment_time; ?>
                                  </a>
                                </td>
                              <td>
                                  <?php $latestApptStat = $lead->leadLatestApptStat(['apptStatus' => $apptStatus]); 
                                  if (!is_null($latestApptStat)) : ?>
                                    <a href="<?=$link?>"  data-toggle="tooltip" title="Click to view more">
                                    <strong class="<?php 
                                      echo !empty($lead->lead_latest_status) ? 
                                        $this->Common->getLeadStatusTextClass($latestApptStat) 
                                              : ''; ?>">
                                          <?php echo  $this->Common->getStatusText($latestApptStat); ?> 
                                      </strong>
                                    </a>
                                  <?php endif; ?>
                              </td>
                          </tr>
                          <?php endforeach; ?>
                         
                      </tbody>
                    </table>
                      </div>
                  </div>
                </div>
              </div>
            </div>
            <?php echo $this->element('pagination'); ?>
          </div>
        </div>
        <!-- /page content -->
        <?php echo $this->Html->script(['jquery.floatThead.min'], ['block' => 'pluginJs']); ?>
        <?php $this->append('scriptBottom'); ?>
        <script type="text/javascript">
            jQuery('document').ready(function(){
            });
        </script>
        <?php $this->end(); ?>