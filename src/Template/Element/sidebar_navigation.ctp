<div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo $this->Url->build('/'); ?>" class="site_title"><i class="fa fa-home"></i> 
                  <span>Our Patient Portal</span>
              </a>
            </div>
            <div class="clearfix"></div>
            <!-- menu profile quick info -->
            <div class="profile">
              <div class="profile_pic">
                  <?php echo $this->Html->image('../images/user.png', ['class' => 'img-circle profile_img']); ?>
              </div>
              <div class="profile_info">
                <span>Welcome,</span>
                <h2><?php echo ($this->AuthUser->hasRoles([ROLE_PHARMACY, ROLE_TELEMEDCINE])) ? 
                        $this->AuthUser->user('company'): $this->AuthUser->user('firstname'); ?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->
            
            <br/>
            <div class="clearfix"></div>
            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <ul class="nav side-menu">
                    <?php if ($this->AuthUser->hasRoles([ROLE_ADMIN, ROLE_STAFF])): ?>
                    <li><a><i class="fa fa-user-md"></i> Doctors <span class="fa fa-chevron-down"></span></a>
                      <ul class="nav child_menu">
                        <li><a href="<?php echo $this->Url->build(['controller' => 'Doctors', 'action' => 'index']); ?>">List Doctors</a></li>
                        <li><a href="<?php echo $this->Url->build(['controller' => 'Doctors', 'action' => 'add']); ?>">Add New Doctor</a></li>
                      </ul>
                    </li>
                    <?php endif; ?>
                    <?php if ($this->AuthUser->hasAccess(['controller' => 'Appointments', 'action' => 'index'])): ?>
                        <li><a href="<?php echo $this->Url->build(['controller' => 'Appointments', 'action' => 'index']); ?>"><i class="fa fa-book"></i> Appointments</a></li>
                    <?php endif; ?>
                        <?php if ($this->AuthUser->hasAccess(['controller' => 'PharmacyLeads', 'action' => 'index'])
                                || $this->AuthUser->hasAccess(['controller' => 'PharmacyLeads', 'action' => 'add'])): ?>
                        <li><a><i class="fa fa-plus-square"></i> Pharmacy Leads <span class="fa fa-chevron-down"></span></a>
                            <ul class="nav child_menu">
                              <?php if ($this->AuthUser->hasAccess(['controller' => 'PharmacyLeads', 'action' => 'index'])): ?>
                                <li><a href="<?php echo $this->Url->build(['controller' => 'PharmacyLeads', 'action' => 'index']); ?>">List Pharmacy Leads</a></li>
                                <?php endif; ?>
                                <?php if ($this->AuthUser->hasAccess(['controller' => 'PharmacyLeads', 'action' => 'add'])): ?>
                                <li><a href="<?php echo $this->Url->build(['controller' => 'PharmacyLeads', 'action' => 'add']); ?>">Add Pharmacy Lead</a></li>
                                <?php endif; ?>
                            </ul>
                        </li>
                    <?php endif; ?>
                        <?php if (!$this->AuthUser->hasRole(ROLE_DOCTOR)): ?>
                        <li><a><i class="fa  fa-files-o"></i> Submissions <span class="fa fa-chevron-down"></span></a>
                          <ul class="nav child_menu">
                                <li><a href="<?php echo $this->Url->build(['controller' => 'Leads', 'action' => 'index']); ?>">List Submissions</a></li>
                              <?php if ($this->AuthUser->hasAccess(['controller' => 'Leads', 'action' => 'add'])): ?>
                                <li><a href="<?php echo $this->Url->build(['controller' => 'Leads', 'action' => 'add']); ?>">Add New Submission</a></li>
                              <?php endif; ?>
                           </ul>
                        </li>
                        <?php endif; ?>
                    <li id="sent_to_tm_li">
                        <a href="<?php echo $this->Url->build(['controller' => 'Leads', 'action' => 'index', '?' => ['tab_view' => 'sentToTm']]); ?>">
                           <i class="fa fa-medkit"></i> <?php echo $this->AuthUser->hasRole(ROLE_DOCTOR) ? 'Patients' : 'Sent to TM'; ?></a>
                    </li>
                    <?php if ($this->AuthUser->hasRole(ROLE_DOCTOR)): ?>
                         <li class="<?php echo $this->Common->getLinkActiveClass('Events'); ?>">
                                <a href="<?php echo $this->Url->build(['controller' => 'Events', 'action' => 'index']); ?>"><i class="fa fa-calendar"></i> My Calendar</a>
                         </li>
                         <li class="<?php echo $this->Common->getLinkActiveClass('TrainingMaterials'); ?>">
                                <a href="<?php echo $this->Url->build(['controller' => 'TrainingMaterials', 'action' => 'index']); ?>">
                                   <i class="fa fa-graduation-cap"></i> Training</a>
                            </li>
                    <?php endif; ?>
                </ul>
              </div>
            </div>
          </div>
        </div>