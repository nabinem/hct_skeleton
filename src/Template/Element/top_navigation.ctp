<?php $curController = $this->request->param('controller');
      $curAction = $this->request->param('action'); ?>
<!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav class="" role="navigation">
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>
                
                <!--Horizontal top Menu-->
                <button type="button" id="horiz_topmenu_toggle" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#horiz_topmenu" aria-expanded="false">
                    <i class="fa fa-bars"></i>
                </button>
                <div class="collapse navbar-collapse pull-left" id="horiz_topmenu">
                    <ul class="nav navbar-nav">
                        <?php if (!$this->AuthUser->hasRole(ROLE_DOCTOR) && 
                                $this->AuthUser->hasAccess(['controller' => 'Events', 'action' => 'index'])): //for doctor this link is shown at left navigation?>
                            <li class="<?php echo $this->Common->getLinkActiveClass('Events'); ?>">
                                <a href="<?php echo $this->Url->build(['controller' => 'Events', 'action' => 'index']); ?>"><i class="fa fa-calendar"></i> My Calendar</a>
                            </li>
                        <?php endif; ?>
                        <?php if ($this->AuthUser->hasAccess(['controller' => 'UserNotes', 'action' => 'add'])): ?>
                            <li role="presentation" class="dropdown notes-dropdown <?php echo $this->Common->getLinkActiveClass('UserNotes'); ?>">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                  <i class="fa fa-calendar-minus-o"></i> My Notes
                                </a>
                                <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                                    <li><b>Recent Notes</b> 
                                        <button data-target="#quick_note_modal" data-toggle="modal" style="margin-left: 15px; font-size: 11px;" class="btn btn-xs btn-primary" id="add_quick_note">
                                            <i class="fa fa-plus"></i> Add quick Note</button></li>
                                    <li id="recent_notes_cont">
                                        <?php if (!empty($recentNotes)){
                                            echo $this->element('../UserNotes/recent_notes');
                                        } ?>
                                    </li>
                                  <li>
                                    <div class="text-center">
                                      <a href="<?php echo $this->Url->build(['controller' => 'UserNotes', 'action' => 'index']); ?>">
                                        <strong>See All Notes</strong>
                                        <i class="fa fa-angle-right"></i>
                                      </a>
                                    </div>
                                  </li>
                                </ul>
                            </li>
                        <?php endif; ?>
                        <?php if ($this->AuthUser->hasAccess(['controller' => 'Tasks', 'action' => 'index'])): ?>
                            <li class="<?php echo $this->Common->getLinkActiveClass('Tasks'); ?>">
                                <a href="<?php echo $this->Url->build(['controller' => 'Tasks', 'action' => 'index']); ?>">
                                    <i class="fa fa-tasks" aria-hidden="true"></i> 
                                    <?php echo $this->AuthUser->hasRole(ROLE_ADMIN) ? 'Tasks' : 'My Tasks'; ?></a>
                            </li>
                        <?php endif; ?>
                        <?php if ($this->AuthUser->hasRole(ROLE_ADMIN)): ?>
                            <li class="<?php echo $this->Common->getLinkActiveClass('TrainingMaterials'); ?>">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                    <i class="fa fa-graduation-cap"></i> Training <i class="fa fa-chevron-down"></i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                  <li class="<?php echo $this->Common->getLinkActiveClass('TrainingMaterials', 'index'); ?>">
                                      <a href="<?php echo $this->Url->build(['controller' => 'TrainingMaterials', 'action' => 'index']); ?>">List All</a>
                                  </li>
                                  <li class="<?php echo $this->Common->getLinkActiveClass('TrainingMaterials', 'add'); ?>">
                                      <a href="<?php echo $this->Url->build(['controller' => 'TrainingMaterials', 'action' => 'add']); ?>">Add New </a>
                                  </li>
                                </ul>
                            </li>
                            <?php elseif (!$this->AuthUser->hasRole(ROLE_DOCTOR)): ?>
                            <li class="<?php echo $this->Common->getLinkActiveClass('TrainingMaterials'); ?>">
                                <a href="<?php echo $this->Url->build(['controller' => 'TrainingMaterials', 'action' => 'index']); ?>">
                                   <i class="fa fa-graduation-cap"></i> Training</a>
                            </li>
                        <?php endif; ?>
                        <?php if ($this->AuthUser->hasRole(ROLE_ADMIN)): ?>
                            <li class="<?php echo $this->Common->getLinkActiveClass('Users', ['index', 'add']); ?>">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-user-secret"></i> Users <i class="fa fa-chevron-down"></i></a>
                            <ul class="dropdown-menu">
                              <li class="<?php echo $this->Common->getLinkActiveClass('Users', 'index'); ?>">
                                  <a href="<?php echo $this->Url->build(['controller' => 'Users', 'action' => 'index']); ?>">List Users</a>
                              </li>
                              <li class="<?php echo $this->Common->getLinkActiveClass('Users', 'add'); ?>">
                                  <a href="<?php echo $this->Url->build(['controller' => 'Users', 'action' => 'add']); ?>">Add New User</a>
                              </li>
                            </ul>
                        </li>
                    <?php endif; ?>
                    <?php if ($this->AuthUser->hasAccess(['controller' => 'Users', 'action' => 'affiliates'])): ?>
                        <li class="<?php echo $this->Common->getLinkActiveClass('Users', 'affiliates'); ?>">
                            <a href="<?php echo $this->Url->build(['controller' => 'Users', 'action' => 'affiliates']); ?>"><i class="fa fa-users"></i> Affiliates</a></li>
                    <?php endif; ?>
                    </ul> 
                </div>
       <!--//Horizontal top Menu--> 
       <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <?php echo $this->Html->image('../images/user.png'); ?>
                        <?php echo ($this->AuthUser->hasRoles([ROLE_PHARMACY, ROLE_TELEMEDCINE])) ? 
                        $this->AuthUser->user('company'):$this->AuthUser->user('firstname'); ?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li>
                        <?php $editLink = $this->AuthUser->hasRole(ROLE_DOCTOR) ? $this->Url->build(['controller' => 'Doctors', 'action' => 'edit']) :  $this->Url->build(['controller' => 'Users', 'action' => 'editProfile']); ?>
                        <a href="<?php echo $editLink; ?>"> 
                            Edit Profile</a></li>
                    <li>
                        <a href="<?php echo $this->Url->build(['controller' => 'Users', 'action' => 'changePassword']); ?>"> 
                            Change Password</a></li>
                    <li><a href="<?php echo $this->Url->build(['controller' => 'Users', 'action' => 'logout']); ?>"><i class="fa fa-sign-out pull-right"></i> Log Out</a></li>
                  </ul>
                </li>
                
                <li role="presentation" class="dropdown">
                  <a href="javascript:;" class="dropdown-toggle info-number" data-toggle="dropdown" aria-expanded="false">
                    <i class="fa fa-bell-o"></i>
                    <!--<span class="badge bg-red">6</span>-->
                  </a>
                  <ul id="menu1" class="dropdown-menu list-unstyled msg_list" role="menu">
                      <li><b>All Notifications</b></li>
                    <li></li>
                  </ul>
                </li>
                
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->
        
        <!--Quick Notes Modal Form starts-->
            <div id="quick_note_modal" class="modal fade" role="dialog">
              <div class="modal-dialog">
                   <?php
                    echo $this->Form->create(null, ['class' => 'form-horizontal quick_note_form', 'templates' => ['inputContainer' => '{{content}}'], 
                        'id' =>  'quick_note_form', 'url' => ['controller' => 'UserNotes', 'action' => 'ajaxAddEdit']
                        ]) ?>
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Enter a quick note: <span id="qknote_autosave_text" class="text-success"></span></h4>
                  </div>
                  <div class="modal-body">
                      <div class="col-md-12">
                        <div class="form-group">
                            <div class="col-md-12">
                                <?php echo $this->Form->input('note', ['required' => true, 'id' => 'quick_note',
                                        'div' => false, 'label' => false, 'class' => 'form-control ckeditor basic_editor']); ?>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="col-md-12"><div class="separator"></div></div>
                  </div>
                  <div class="modal-footer">
                      <button type="submit" style="margin-bottom:0;" class="btn btn-success" data-loading-text="Saving..">Save</button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
                  </div>
                </div>
               <?php echo $this->Form->end(); ?> 

              </div>
            </div> 
        <!--Quick note Modal Form ends-->
        <?php echo $this->element('ckeditor'); ?>
        <?php $this->append('scriptBottom'); ?>
        <script type="text/javascript">
            jQuery(document).ready(function(){
                //quick note autosave
                var qkNoteAsTmo;
                var $qkNoteAsTxt = $('#qknote_autosave_text');
                var $qkNoteForm = $('#quick_note_form'); 
                CKEDITOR.instances.quick_note.on('change', function(){
                    $qkNoteForm.addClass('dirty');
                    if (qkNoteAsTmo) clearTimeout(qkNoteAsTmo);
                    qkNoteAsTmo = setTimeout(function () {
                        $qkNoteAsTxt.html('Saving...');
                        var quickNote =  CKEDITOR.instances.quick_note.getData();
                        $qkNoteForm.find('#quick_note').val(quickNote);
                        jQuery.ajax({
                            url: $qkNoteForm.attr('action'),
                            type: 'POST',
                            data: $qkNoteForm.serialize(),
                            dataType: 'json',
                            success: function(json) {
                                if (json.ajaxStatus == 'success'){
                                    var $qkNoteId = $qkNoteForm.find('#qk_note_id');
                                    if ($qkNoteId.length){
                                        $qkNoteId.val(json.id);
                                    } else {
                                       $qkNoteForm.append('<input type="hidden" id="qk_note_id" name="id" value="'+json.id+'">');
                                    }
                                    var now = new Date();
                                    var timeText = now.getMonth()+'/'+now.getDate()+'/'+now.getFullYear()+' '+now.getHours()+':'+now.getMinutes()+':' + now.getSeconds();
                                    $qkNoteAsTxt.html('Autosaved on ' + timeText);
                                    $('#recent_notes_cont').html('').html(json.content);
                                    $qkNoteForm.removeClass('dirty');
                                } else {
                                   $qkNoteAsTxt.html('');
                                }
                            },
                        });
                    }, 3000);
                });
                //If Modal closed force submit if still dirty
                $('#quick_note_modal').on('hidden.bs.modal', function () {
                    if ($qkNoteForm.hasClass('dirty')){
                        var quickNote =  CKEDITOR.instances.quick_note.getData();
                        if ($.trim(quickNote) != ''){
                            $qkNoteForm.submit();
                        }
                    } else {//if not dirty reset the form
                        CKEDITOR.instances.quick_note.setData('');
                        //remove note id
                        var $qkNoteId = $qkNoteForm.find('#qk_note_id');
                        if ($qkNoteId.length){
                            $qkNoteId.remove();
                        }
                        $qkNoteForm.find('#quick_note').val('');
                        $('#qknote_autosave_text').html('');
                    }
                });
                
                //quick note add edit form
                $qkNoteForm.submit(function(e){
                    var quickNote =  CKEDITOR.instances.quick_note.getData();
                    if ($.trim(quickNote) == ''){
                        alert('Note cannot be empty.');
                        return false;
                    }
                    //Ajax submit
                    $(this).find('#quick_note').val(quickNote);
                    var $submitBtn = $(this).find('button[type="submit"]');
                    jQuery.ajax({
                        url: $(this).attr('action'),
                        type: 'POST',
                        data: $(this).serialize(),
                        dataType: 'json',
                        beforeSend: function() {
                            $submitBtn.button('loading');
                        },
                        complete: function() {
                            $submitBtn.button('reset');
                        },
                        success: function(json) {
                            if (json.ajaxStatus == 'success'){
                               $(this).trigger("reset");
                               CKEDITOR.instances.quick_note.setData('');
                               //remove note id
                               var $qkNoteId = $(this).find('#qk_note_id');
                               if ($qkNoteId.length){
                                   $qkNoteId.remove();
                               }
                               $(this).find('#quick_note').val('');
                               $('#qknote_autosave_text').html('');
                               $('#recent_notes_cont').html('').html(json.content);
                               toastr.success('Note saved successfully.');
                               $("#quick_note_modal").modal('hide');
                            } else {
                               toastr.error('Something went wrong. Try again.'); 
                            }
                        },
                        error: function(){ toastr.error('Something went wrong. Try again.'); }
                    });
                    
                    return false;
                    
                });
                
            });
        </script>
    <?php $this->end(); ?>