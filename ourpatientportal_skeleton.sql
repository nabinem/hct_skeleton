-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 06, 2017 at 10:52 पूर्वाह्न
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ourpatientportal_skeleton`
--

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `company` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `forgot_pass_token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `forgot_pass_expire` datetime DEFAULT NULL,
  `role_id` tinyint(2) DEFAULT NULL COMMENT ' 1 => Admin, 2 => Staff, 3 => 1 => Affiliates',
  `active` tinyint(1) NOT NULL COMMENT '0 => inactive , 1 => active',
  `last_login` datetime DEFAULT NULL,
  `last_login_ip` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `deleted` datetime DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `company`, `username`, `email`, `password`, `forgot_pass_token`, `forgot_pass_expire`, `role_id`, `active`, `last_login`, `last_login_ip`, `deleted`, `created`, `modified`) VALUES
(1, 'Admin ', 'HCT', '', 'admin', 'admin@ourpatientportal.com', '$2y$10$LHFfOxxnuXayE/4w4pF0KOocb49hQKM4Nw.svIdodQTy1KZBw/V/C', '', NULL, 1, 1, '2017-06-06 04:29:46', '127.0.0.1', NULL, '2016-10-19 04:39:18', '2017-06-06 04:29:46'),
(165, 'staff', 'staff', '', 'staff', 'staff@ourpatientportal.com', '$2y$10$SlDl0fgUCaqJrrRUoHkVeeS3tZtc1p4gMMnmI6kVvftSS96X66wDK', '', NULL, 2, 1, '2017-06-06 04:10:28', '127.0.0.1', NULL, '2017-06-06 04:10:00', '2017-06-06 04:10:28'),
(166, 'Affiliate', 'aa', '', 'affiliate', 'affiliate@affiliate.com', '$2y$10$lkqMR/v22GSQIXyw2QW6j.SrTgAtJ037MiutjDHbNJtNHbQcdUTDu', '', NULL, 3, 1, '2017-06-06 04:23:28', '127.0.0.1', NULL, '2017-06-06 04:23:16', '2017-06-06 04:23:28');

-- --------------------------------------------------------

--
-- Table structure for table `user_profiles`
--

CREATE TABLE `user_profiles` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `zip` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `user_profiles`
--

INSERT INTO `user_profiles` (`id`, `user_id`, `address`, `city`, `state`, `zip`, `country`, `created`, `modified`) VALUES
(1, 166, 'Dhumbarahi', 'Kathmandu', 'Bamati', '977', 'NP', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `deleted` (`deleted`);

--
-- Indexes for table `user_profiles`
--
ALTER TABLE `user_profiles`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=167;
--
-- AUTO_INCREMENT for table `user_profiles`
--
ALTER TABLE `user_profiles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
